This is the project home for the Inflation Prediction Project.

## Installation Instructions
* Get and setup Anaconda's minimal Miniconda --
  `wget http://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -q -O miniconda.sh`
  `bash miniconda.sh -b -p "$HOME/miniconda3"`
  `echo export PATH="$HOME/miniconda3/bin:$PATH" >> ~/.bashrc`
  `source ~/.bashrc`
  `conda update --quiet --yes conda`


* Setup the Anaconda Environment from the yml environment file for this project--
  `conda env create -f environment.yml`

* Activate the Anaconda Environment --
  `conda activate inflation_prediction`

* Export the current Anaconda Environment -- (rarely used, only if you want a
  new environment file due to changes in the modules used)
  `conda env export > environment.yml` and then delete the line starting with
  Prefix as that is specific to your computer

* Insall nltk punkt module -- 
  From the python2 interpreter, 
  `>> import nltk`
  `>> nltk.download('punkt')`

* Install English Language packs for Spacy --
  `python -m spacy download en_core_web_sm`
  `python -m spacy download en`

* Optionally install markdown viewer for terminal
  (https://github.com/axiros/terminal_markdown_viewer)
  `pip install mdv`


## How to Generate Inflation Reports:

How to Generate Inflation Report:

Open Ubuntu Terminal : ctrl alt t
go to folder: "cd iimc"

note: all folder locations are relative to the "iimc" folder.

1. Update data/CPI_data.csv
    First update data/CPI_data.csv from mospi website http://164.100.34.62:8080/

### Crawling and getting the data setup:

NEW METHOD: Crawling ET directly


1. Go to `/storage/finlab_06/data/EconomicTimes` , read the README.md there and
   make sure you crawl the news of necessary months AND convert it to json.
   The output can be seen in /output/html and /output/json in the above folder.

2. Get articles of interest from (20th of last month to 10th of current month)
    a. run "python2 JsonToArticle.py"

This creates our dataset in /storage/finlab_06/month_20_10
    
OLD METHOD: Factiva Crawl of ET and BL corpus; may not work anymore | UNRELIABLE


1. Crawl from factiva
    a. Change month to crawl for in config/update_month.txt (USE ONLY ONE MONTH AT A TIME)
    b. Check manually if factiva is working. Click on factiva.com on http://library.iimcal.ac.in/html/elecdball.php?lst=alp
    c. run "python2 Factiva_crawler_3.py"

2. Convert crawled pdf to raw txt files
    a. run "python2 processFileWin.py"

3. Get articles of interest from (20th of last month to 10th of current month)
    a. run "python2 PrintMonthData.py"

This, unlike the method above, creates the dataset in /data/month_20_10 in this
repository. So if you use this method, you will have to update the location of
data files in GetMonthData.py (and maybe some other places).

### Running the NLP pipeline:

1. Update the months in config/update_month.txt for each month for which you
   want the NLP pipeline to be run. So, at the start, you may want to include
   ALL months starting from 201208 (which is when we have our data from) upto
   the month of interest. Once you've run the pipeline on everything in the
   past, for each new run, all you need to do is just add the new month for
   which you want to run the pipeline. The entire pipeline below runs on all
   months in config/update_month.txt, and InflationPrediction.py will give you
   'cumulative' predictions until the last listed month. Please do not list
   months out of order. If you don't want to run the entire pipeline, the output
   folder from kanodiaayush's $HOME/iimc/output maybe copied to your repo and
   then you just need to run the pipeline on additional months.

2. Topic Classifier
    a. run "python2 ClassifyTopic.py"
    b. run "python2 ClassifyTopic_Supervised.py"
    c. run "python2 ClassifyTopic_Both.py"

3. Price Production classifier
    a. run "python2 PriceOrProduction.py"

4. Negation
    a. run "python2 AddNegation.py"

5. Sentiment Detection
    a. run "python2 SentimentAssigner.py"

6. Predictor
    a. run "python2 InflationPredictor.py"
    Note: If you're running this via tmux, you may see an error like:
    `qt.qpa.screen: QXcbConnection: Could not connect to display localhost:11.0
    Could not connect to any X display.`
    In this case, note that first, your SSH session should alredy be X11 enabled
    to begin with (that is you should start it with -X or -Y), and then, set the
    correct DISPLAY variable value in tmux. There are probably more ways to do
    this, but one way is to check the value in bash (minus tmux) (`echo
    $DISPLAY`) and then setting that value in tmux (`export DISPLAY=:10.0`) (or
    such). If this does not work for you, google the error and fix it.


Additional -- To calculate idfs of sentiment words
    a. run "python2 IDF_Calculator.py"
