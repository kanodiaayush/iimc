import numpy as np
import GetMonthData as md

WORDS_DIR = "data/words"
TEXT_FILES = "output/topic_classified_both"
TEXT_FILES_INFLATION = "output/topic_classified"

def TopicStats(topic):
  month_ends, inflation = md.GetMonthsAndInflation()
  num_articles = list()
  for year, month in month_ends:
    if topic == "inflation":
      text_files = TEXT_FILES_INFLATION
    else:
      text_files = TEXT_FILES
    filebase = '%s/%02d_%02d_%s_ratings' % (text_files, month, year, topic)
    filename = filebase + '.txt'
    inputfile = open(filename, 'r')
    count = 0
    for line in inputfile.readlines():
      count += 1
    num_articles.append(count)
  print ("%s\t%f\t%f\t%d\t%d" % (topic, np.mean(num_articles), np.median(num_articles), np.min(num_articles), np.max(num_articles)))

if __name__ == "__main__":
  print ("topic\tmean\tmedian\tmin\tmax")
  for topic in ["fuel", "food", "cloth", "house", "pan", "misc","inflation"]:
    TopicStats(topic)
