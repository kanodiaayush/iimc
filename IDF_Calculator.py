import nltk
import os
import random as rand
import math
import pickle

import ClassifyTopic as ct
import GetMonthData as md

WORDS_DIR = "data/words"
OUT_DIR = "output/idf"

def idf(counts, N):
  return [5.0 if count == 0 else math.log(N/count) for count in counts]

def MonthlyIDF(article_list, words, stemmer):
  t = dict()
  for (i, word) in enumerate(words):
    t[word] = i
  count_for_word = [0 for word in words]
  N = len(article_list)
  for article in article_list:
    this_article = [False for word in words]
    for word in nltk.word_tokenize(article):
      for i in xrange(len(words)):
        if word == words[i]:
          this_article[i] = True
    for i in xrange(len(count_for_word)):
      if this_article[i]:
        count_for_word[i] += 1
    
  return zip([stemmer.stem(word) for word in words], idf(count_for_word, N))

if __name__ == "__main__":
  increase_words = ct.ReadWordsNoStemming("%s/increase.txt" % WORDS_DIR)
  decrease_words = ct.ReadWordsNoStemming("%s/decrease.txt" % WORDS_DIR)
  articles, month_ends, inflation = md.GetAllData(IDF=True)
  month_articles = articles[0] + articles[1] + articles[2] + articles[3] + articles[4] + articles[5]
  sno = nltk.stem.SnowballStemmer('english')
  increase_IDF = MonthlyIDF([article.decode('utf-8-sig') for article in month_articles.splitlines()], increase_words, sno)
  decrease_IDF = MonthlyIDF([article.decode('utf-8-sig') for article in month_articles.splitlines()], decrease_words, sno)
  pickle.dump(increase_IDF, open('%s/increase_IDF.p' % OUT_DIR, 'wb'))
  pickle.dump(decrease_IDF, open('%s/decrease_IDF.p' % OUT_DIR, 'wb'))
  print increase_IDF
  print decrease_IDF
    
