import csv
import fnmatch
import re
import os
import pandas as pd
import numpy as np

import GetCPIWPI as infl
from GetConfig import UpdateMonths

months = UpdateMonths()
if len(months) != 1:
    raise Exception("Only one month can be crawled at a time")
yearmonth = months[0]
month = yearmonth % 100
year = yearmonth / 100

DATA_DIR = "factiva_extracted/%d" % yearmonth
OUT_DATA_DIR = "data/month_20_10"

def GetArticle(article_name):
  f = open("%s/%s" % (DATA_DIR, article_name), 'r')
  return f.read()

def GetHeadline(article_name, headlines, newspaper):
  return headlines[GetUniqueKey(article_name) - 1]
    
def GetUniqueKey(article_name):
  pattern = re.compile(".*_([0-9]*).txt")
  return int(pattern.match(article_name).group(1))
  
def GetArticleNames(date_month_year, newspaper):
  date = date_month_year[0]
  month = date_month_year[1]
  year = date_month_year[2]
  article_names = list(str())
  #print date_month_year
  f_reg = "*_%04d%02d%02d_%s_*.txt" % (year, month, date, newspaper)
  #print f_reg
  for article_name in os.listdir(DATA_DIR):
    if fnmatch.fnmatch(article_name, f_reg):
      article_names.append(article_name)
  #print article_names
  return article_names
      

def GetDateMonthPairs(month, year):  
  date_month_years = list()
  if (month == 1):
    month = 12
    year -= 1
  else:
    month -= 1
  for i in xrange(20, 32):
    date_month_years.append((i, month, year))
  if (month == 12):
    month = 1
    year += 1
  else:
    month += 1
  for i in xrange(1, 11):
    date_month_years.append((i, month, year))
  return date_month_years

def GetHeadlines(newspapers):
  headlines = list()
#  for newspaper in newspapers:
  with open("%s/metadata.csv" % DATA_DIR) as csvfile:
    inforeader = csv.reader(csvfile, delimiter = ',')
    df = pd.read_csv(csvfile)
    headlines = [df.HEADLINE[i] for i in xrange(len(df.HEADLINE))]
  return headlines

def PrintMonthData(month, year, newspapers, headlines, newspaper_map):
  date_month_years = GetDateMonthPairs(month, year)
  for newspaper in newspapers:
    month_data = str()
    for date_month_year in date_month_years:
      article_names = GetArticleNames(date_month_year, newspaper)
      for article_name in article_names:
        headline = GetHeadline(article_name, headlines, newspaper).lower()
        month_data += headline

        month_data += '|||'
        month_data += GetArticle(article_name).lower()
        month_data += '\n'
    f = open("%s/%04d%02d_%s.txt" % (OUT_DATA_DIR, year, month, newspaper_map[newspaper]), 'w')
    f.write(month_data)
    
MONTH_END, Inflation, _, _ = infl.GetAllInflation()
newspapers = ['Business Line', 'Economic Times']
newspaper_map = dict()
newspaper_map['Business Line'] = 'Business_Line'
newspaper_map['Economic Times'] = 'Economic_Times'
headlines = GetHeadlines(newspapers)
print MONTH_END
#MONTH_END = [201805, 201806, 201807]
#MONTH_END = [201811]
#MONTH_END = UpdateMonths()
#for month_end in MONTH_END:
  #year = month_end // 100
  #month = month_end % 100
  #PrintMonthData(month, year, newspapers, headlines, newspaper_map)


PrintMonthData(month, year, newspapers, headlines, newspaper_map)
print ("Done " + str(month) + str(year))
