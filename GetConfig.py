import csv
import pandas as pd

def UpdateMonths():
    with open('config/update_month.txt') as csvfile:
        inforeader = csv.reader(csvfile, delimiter=',')
        df = pd.read_csv(csvfile)
        yearmonths = list(df.YYYYMM)
        yearmonths = [int(elem) for elem in yearmonths]
        return (yearmonths)

