import pickle
import nltk
import numpy as np
import spacy

import PriceOrProduction as pp
import GetMonthData as md

SENTENCE_TAPER = .50

TEXT_FILES = "output/negation"
IDF_DIR = "output/idf"
OUT_DIR = "output/sentiment"
NLP = spacy.load('en')

DIST_FROM_SUBJECT_DAMP = 0.85
def GetArticles(topic, month, year):
  return pickle.load(open("%s/%02d_%02d_%s_ratings.p" % (TEXT_FILES, month, year, topic), 'r'))

def GetIDF():
  increase_idf = pickle.load(open("%s/increase_IDF.p" % IDF_DIR, 'r')) 
  decrease_idf = pickle.load(open("%s/decrease_IDF.p" % IDF_DIR, 'r')) 
  return increase_idf, decrease_idf
    
def CheckNegation(index, indices):
  for a,b in indices:
    if a <= index and index < b:
      return -1.0
  return 1.0

def GetMinDist(index, indices):
  if not indices:
    print ("ERROR: THIS SHOULD NEVER BE HIT")
    return 1
  return min(indices, key=lambda x:(abs(x - index)))
  
def ValInLine(line, indices, words, stemmer, subject_indices):
  ans = 0.0
  span_generator = nltk.WhitespaceTokenizer().span_tokenize(line)
  count = 0
  #for token in nltk.word_tokenize(line):
    #for word, val in words:
      #if stemmer.stem(token) == word:
        #ans += val
  for a,b in span_generator:
    token = line[a:b]
    for word, val in words:
      if stemmer.stem(token) == word:
        ans += (val * CheckNegation(a, indices) * np.power(DIST_FROM_SUBJECT_DAMP, GetMinDist(count, subject_indices)))
    count += 1
  return ans

def SentenceSentimentPositive(line, indices, idfs, stemmer, subject_indices):
  ans = 0.0
  idfs = increase_idf + [(word, val * -1.0) for word,val in decrease_idf]
  ans += ValInLine(line, indices, idfs, sno, subject_indices) 
  #ans += 1.0
  #doc = NLP(line)
  return ans

def sigmoid(num):
  return 1.0/(1.0 + np.exp(-num))
def ArticleSentiment(article, idfs, stemmer):
  sentence_sentiments = list()
  k = 1.0
  count = 0
  exceptions = ["tea", "sugar", "cocoa"]
  for line, label, indices, price_indices, production_indices in article:
    if count == 0:
      for elem in exceptions:
        if elem in line:
          return 0.0, sentence_sentiments
    value = 0.0
    if count > 4:
      pass 
    elif label == 1: 
      sentiment = k * SentenceSentimentPositive(line, indices, idfs, stemmer, price_indices)
      value = sentiment
    elif label == 2:
      sentiment = k * SentenceSentimentPositive(line, indices, idfs, stemmer, production_indices) * -1.0
      value = sentiment
    elif label == 3:
      sentiment_price = k * SentenceSentimentPositive(line, indices, idfs, stemmer, price_indices)
      sentiment_production = k * SentenceSentimentPositive(line, indices, idfs, stemmer, production_indices) * -1.0
      value = sentiment_price + sentiment_production
    k *= SENTENCE_TAPER
    count += 1
    sentence_sentiments.append(value)
  num_sentiments = np.count_nonzero(sentence_sentiments)
  if num_sentiments == 0:
    ans = 0.0
  #elif sum(sentence_sentiments) >= 1:
    #ans = 1.0
  #elif sum(sentence_sentiments) <= -1:
    #ans = -1.0
  else:
    ans = sigmoid(sum(sentence_sentiments))  - 0.5
  return ans, sentence_sentiments
    
    
if __name__ == "__main__":
  #TODO pull out topic to be common for various modules  
  sno = nltk.stem.SnowballStemmer('english')
  #for topic in ["pan"]:
  for topic in ["fuel", "food", "cloth", "house", "pan", "misc", "inflation"]:
    month_ends = md.GetAllMonthEnds()
    for year, month in month_ends:
      #if year != 2015 or month != 12:
        #continue
      articles = GetArticles(topic, month, year)
      sentiments = list()
      filebase = "%s/%02d_%02d_%s_ratings" % (OUT_DIR, month, year, topic)
      filename = filebase + '.html'
      picklename = filebase + '.p' 
      increase_idf, decrease_idf = GetIDF()
      idfs = increase_idf + [(word, val * -1.0) for word,val in decrease_idf]
      count = 0
      sentence_sentiments = list()
      for article in articles:
        sentiment, sentence_sentiment = ArticleSentiment(article, idfs, sno)
        sentiments.append(sentiment)
        sentence_sentiments.append(sentence_sentiment)
        count += 1
      f = open(filename, 'w')
      f.write("<head> %s <body>" % topic)
      if len(sentiments) > 0:
        sentiments, sentence_sentiments, articles = (list(t) for t in (zip(*sorted(zip(sentiments, sentence_sentiments, articles), reverse=True, key= lambda x: abs(x[0])))))
      for i, article in enumerate(articles):
        pp.PrintArticle(article, f, sentence_sentiments[i])
        f.write("%f\n" % sentiments[i])
      f.write("</body> </head>")
      pickle.dump(zip(articles, sentiments, sentence_sentiments), open('%s.p' % filebase, 'wb'))
      print ("%02d %02d %s done" % (month, year, topic))
    #print (topic + " done")

