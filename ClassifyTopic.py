from collections import defaultdict
import re
import nltk

import GetMonthData as md

HEADLINE_SCORE = .9
FIRST_SENTENCE = .8
SENTENCE_TAPER = .04

WORDS_DIR = "data/words"
OUT_DIR = "output/topic_classified"

def WordInLine(line, words, stemmer):
  for token in nltk.word_tokenize(line):
    if stemmer.stem(token) in words:
      return True
  return False

def WordInLineBigramException(line, words, stemmer, bigram_exception):
  prev_stemmed = ""
  for token in nltk.word_tokenize(line):
    stemmed = stemmer.stem(token)
    for word in words:
      if stemmed == word:
        if word in bigram_exception.keys(): 
          if prev_stemmed not in bigram_exception[word]:
            return True
        else:
          return True
    prev_stemmed = stemmed
  return False

def RateArticle(headline, article, words, sno, bigram_exception):
  if "years ago" in headline:
    0.0
  ans = 0.0
  debug = False
  if WordInLineBigramException(headline, words, sno, bigram_exception):
    ans += 0.9
  first_appearance = 1000
  count = 0
  for line in nltk.sent_tokenize(article):
    if WordInLineBigramException(line, words, sno, bigram_exception):
      first_appearance = min(first_appearance, count)
      break
    count += 1
    if count > 3:
      break
  ans += max(0, FIRST_SENTENCE - SENTENCE_TAPER * first_appearance)
  return min(ans, 1.0)

def ReadWords(filename, stemmer):
  f = open(filename, 'r')
  return [stemmer.stem(word.strip()) for word in f.readlines()]

def ReadWordsNoStemming(filename):
  f = open(filename, 'r')
  return [word.strip() for word in f.readlines()]

if __name__ == "__main__":
  sno = nltk.stem.SnowballStemmer('english')
  words = defaultdict(list)
  bigram_exception = defaultdict(dict)
  for topic in ["fuel", "food", "cloth", "house", "pan", "misc", "inflation"]:
  #for topic in ["fuel"]:
  #for topic in ["inflation"]:
    articles, month_ends, inflation = md.GetAllData()
    words[topic] = ReadWords("%s/%s.txt" % (WORDS_DIR, topic), sno)
    print words[topic]
    if topic == "fuel":
      exceptions = ReadWordsNoStemming("%s/%s_exception.txt" % (WORDS_DIR, topic))
      for word in exceptions:
        bigram_exception[topic][sno.stem(word)] = ReadWords("%s/%s_%s.txt" % (WORDS_DIR, topic, word), sno)
    #words = ["crude oil", "petroleum", "petrol", "oil and gas", "natural gas", "shale gas"] 
    #words =  ['pan', 'tobacco', 'liquor', 'intoxicant', 'alcohol', 'bidi', 'cigarette']
    #words = ['clothes', 'clothing', 'footwear', 'sandals', 'slippers', 'chappals', 'dress']
    #words = ['housing', 'estate', 'developers', 'builders', 'real estate']
    #words = ['food', 'drinks', 'beverages'] 
  for i, monthly_article in enumerate(articles):
    article_ratings = defaultdict(list)
    article_list = defaultdict(list)
    year, month = month_ends[i]
    for article in monthly_article.splitlines():
      article = article.decode('utf-8-sig')
      pipe = article.find("|||")
      headline = article[0:pipe]
      body = article[pipe + 4 : -1]
      for topic in ["inflation"]:
      #for topic in ["fuel", "food", "cloth", "house", "pan", "misc", "inflation"]:
        rating = RateArticle(headline, body, words[topic], sno, bigram_exception[topic])
        if rating > 0.85:
          article_ratings[topic].append(rating)
          article_list[topic].append(article)
          break
    for topic in ["inflation"]:
    #for topic in ["fuel", "food", "cloth", "house", "pan", "misc", "inflation"]:
      print topic
      print len(article_ratings[topic])
      print len(article_list[topic])
      if len(article_ratings[topic]) > 0:
        article_ratings[topic], article_list[topic] = [list(t) for t in zip(*sorted(zip(article_ratings[topic], article_list[topic]), reverse = True))]
      f = open("%s/%02d_%02d_%s_ratings.txt" % (OUT_DIR, month, year, topic), 'w')
      for i in xrange(0, len(article_list[topic])):
        f.write("%d " % i)
        f.write(article_list[topic][i].encode('utf8'))
        f.write("%f\n" % article_ratings[topic][i])
      f.close()
      print ("%02d %02d done" % (month, year))

  

  
