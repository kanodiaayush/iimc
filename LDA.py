import numpy as np
import guidedlda
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer

import GetMonthData as md

import NaiveBayes as nb
# TODO(kanodiaayush): Pull utility functions above

DATA_DIR = "data/month_20_10/"


#articles, month_ends, inflation = md.GetAllData()
f = open('all_baskets.txt', 'r')
articles = list()
articles.append(f.read())
#print articles
monthly_article_lists = list()

for monthly_article in articles:
  monthly_list = list()
  #print monthly_article
  for article in monthly_article.splitlines():
    if article != '\n':
      monthly_list.append(article)
  monthly_article_lists.append(monthly_list)
  print len(monthly_list)
  vectorizer = CountVectorizer(token_pattern = '[a-z][a-z]+', stop_words = 'english')
  X = vectorizer.fit_transform(monthly_list).toarray()
  vocab = vectorizer.get_feature_names()
  word2id = dict((v, idx) for idx, v in enumerate(vocab))
  tfidf = TfidfTransformer()
  tfidf.fit(X)
  print X.shape
  print X.sum()
  for count,row in enumerate(X):
    if row.sum() <= 0:
      print monthly_list[count]

  #model = guidedlda.GuidedLDA(n_topics=5, n_iter=100, random_state=7, refresh=20)
  #model.fit(X)
  #topic_word = model.topic_word_
  #n_top_words = 8
  #for i, topic_dist in enumerate(topic_word):
    #topic_words = np.array(vocab)[np.argsort(topic_dist)][:-(n_top_words+1):-1]
    #print('Topic {}: {}'.format(i, ' '.join(topic_words)))

  seed_topic_list = [['crude', 'oil', 'petroleum', 'petrol', 'gas', 'shale', 'diesel'],
        ['pan', 'tobacco', 'liquor', 'intoxicant', 'alcohol', 'bidi', 'cigarette'],
        ['clothes', 'clothing', 'footwear', 'sandals', 'slippers', 'chappals', 'dress'],
        ['housing', 'estate', 'developers', 'builders'],
        ['food', 'drinks', 'beverages']]

  model = guidedlda.GuidedLDA(n_topics=5, n_iter=100, random_state=7, refresh=20)
  seed_topics = {}
  for t_id, st in enumerate(seed_topic_list):
    for word in st:
      if word in word2id:
        seed_topics[word2id[word]] = t_id
      else:
        print (word + " not in vocab (seed word out of dictionary).")
  
  model.fit(X, seed_topics=seed_topics, seed_confidence=1.00)
  #model.fit(X)
  topic_word = model.topic_word_
  n_top_words = 10 
  for i, topic_dist in enumerate(topic_word):
    topic_words = np.array(vocab)[np.argsort(topic_dist)][:-(n_top_words+1):-1]
    print('Topic {}: {}'.format(i, ' '.join(topic_words)))

  doc_topic = model.transform(X)
  for i in range(9):
    print("top topic: {} Document: {}".format(doc_topic[i].argmax(), ', '.join(np.array(vocab)[list(reversed(X[i,:].argsort()))[0:5]])))
    print monthly_list[i]
  break
