import pickle
import nltk
import numpy as np
import re
import spacy
import os

import PriceOrProduction as pp
import GetMonthData as md

SENTENCE_TAPER = .85

TEXT_FILES = "output/price_or_production"
IDF_DIR = "output/idf"
OUT_DIR = "output/negation"

TMP_INP = "input.tmp"
TMP_OUT = "output.tmp"

def GetArticles(topic, month, year):
  return pickle.load(open("%s/%02d_%02d_%s_ratings.p" % (TEXT_FILES, month, year, topic), 'r'))

def AddNegScopes(line, indices):
  numscopes = 0
  currstart = 0
  for i in xrange(len(line) - 7):
    if line[i:i+7] == "<SCOPE>":
      currstart = i
    if line[i:i+8] == "</SCOPE>":
      #indices.append((currstart - numscopes*15, i - (numscopes*15 + 7)))
      indices.append((currstart, i))
      numscopes += 1

def MergeNegScopes(indices):
  flattened_indices = list()
  for a,b in indices:
    flattened_indices.append(a)
    flattened_indices.append(b)
    flattened_indices.sort()
  unique_indices = list()
  i = 0
  while i < len(flattened_indices):
    if i == len(flattened_indices) - 1:
      unique_indices.append(flattened_indices[i])
      break
    elif flattened_indices[i] == flattened_indices[i+1]:
      i += 2
    else:
      unique_indices.append(flattened_indices[i])
      i += 1
  ans = list()
  i = 0
  while i < len(unique_indices):
    ans.append((unique_indices[i], unique_indices[i+1]))
    i += 2
  return ans

    

if __name__ == "__main__":
  #for topic in ["inflation"]:
  month_ends = md.GetAllMonthEnds()
  for topic in ["fuel", "food", "cloth", "house", "pan", "misc", "inflation"]:
    #_, month_ends, _ = md.GetAllData()
    for year, month in month_ends:
      #if year != 2016 or month != 12:
        #continue
      tmpinppath = "%s/%s" % (OUT_DIR, TMP_INP)
      tmpoutpath = "%s/%s" % (OUT_DIR, TMP_OUT)
      tmpinp = file(tmpinppath, 'w')
      lines = dict()
      count = 0
      articles = GetArticles(topic, month, year)
      filebase = "%s/%02d_%02d_%s_ratings" % (OUT_DIR, month, year, topic)
      picklename = filebase + '.p' 
      for article in articles:
        for line, label, _, _ in article:
          if label == 1 or label == 2:
            lines[count] = line
            count += 1
            tmpinp.write(re.sub(u'( |\xa0)+', u' ', line).encode('utf-8'))
            tmpinp.write('\n')
      tmpinp.close()

      cmd = "python2 DepND.py "+ tmpinppath +" "+ tmpoutpath + " > neg_temp.txt"
      os.system(cmd)
      cmd = "rm neg_temp.txt"
      os.system(cmd)
      negatefile = open(tmpoutpath, 'r')
      curr_lines = list()
      count = 0
      negindices = dict()

      for line in negatefile.readlines():
        if "__NONEG" in line:
          negindices[count] = list()
          count += 1
        elif line.rstrip():
          curr_lines.append(line)
        else:
          negindices[count] = list()
          for currline in curr_lines:
            #negindices[count].append((0,0))
            AddNegScopes(currline, negindices[count])
            lines[count] = currline.decode('utf-8')
          negindices[count] = MergeNegScopes(negindices[count])
          count += 1
          curr_lines = list()

      articles_pickle = list()

      count = 0
      for article in articles:
        article_pickle = list()
        for (line, label, price_indices, production_indices) in article:
          if label == 1 or label == 2:
            article_pickle.append((lines[count], label, negindices[count], price_indices, production_indices))
            count += 1
          else:
            article_pickle.append((line, label, list(), price_indices, production_indices))
        articles_pickle.append(article_pickle)

      picklename = filebase + ".p" 
      pickle.dump(articles_pickle, open(picklename, 'wb'))

      print ("%02d %02d %s done" % (month, year, topic))
      os.remove(tmpinppath)
      os.remove(tmpoutpath)
