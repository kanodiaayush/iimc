import re
import spacy
import sys

nlp = spacy.load('en')
text = open(sys.argv[1], 'r').read().decode('utf-8')
split_texts = text.split('\n')
for line in split_texts:
  if "__NONEG" in line:
    print (line)
  else:
    line = re.sub(' +',' ', line)
    doc = nlp(line)
    #doc = nlp_en(u'In contrast, fixed tissue macrophages express detectable HIV proteins, indicating permanent virus transcription.')
    #doc = nlp(u'Diesel prices unlikely to go up soon')
    for sent in doc.sents:
      for i, word in enumerate(sent):
        if word.head == word:
          head_idx = 0
        else:
          head_idx = word.head.i - sent[0].i + 1
        word_str = "%s" % word
        if word_str != "\n":
          line = u"%d\t%s\t%s\t_\t%s\t%s\t%s\t%s"%( i+1, word, word.lemma_, word.tag_, word.ent_type_, str(head_idx), word.dep_)
          print(line.encode('utf-8'))
      print ""
      break
