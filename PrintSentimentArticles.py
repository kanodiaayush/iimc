import GetCPIWPI as cpi
import pickle
TEXT_FILES = "output/sentiment"
OUT_DIR = "data/labelled/sentiment"

def GetArticles(topic, month, year):
  return pickle.load(open("%s/%02d_%02d_%s_ratings.p" % (TEXT_FILES, month, year, topic), 'r'))

if __name__ == "__main__":
  month_ends, _, _, _ = cpi.GetAllInflation()
  topics = ["fuel", "food", "cloth", "house", "pan", "misc"]
  for topic in topics:
    for month_end in month_ends:
      year = month_end // 100
      month = month_end % 100
      if (year, month) in [(2015, 12), (2016,12)]:
        art_count = 0
        f = open("%s/%04d_%02d_%s.txt" % (OUT_DIR, year, month, topic), 'w')
        articles = GetArticles(topic, month, year)
        for article in articles:
          article = article[0]
          toprint = ""
          count = 0
          for line in article:
            (sent, _, _, _, _) = line
            toprint += sent.encode('utf-8')
            count += 1
            if count > 4:
              break
          toprint = toprint.replace('\n', '')
          f.write(toprint + "\n" + "\n")
          art_count += 1
          if art_count >= 50:
            break
        f.close()
