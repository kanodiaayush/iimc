import pickle
import nltk
import numpy as np
from scipy import stats
import sklearn
from matplotlib import pyplot as plt
from sklearn import linear_model
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import LogisticRegression
import re
import pandas as pd
from scipy.stats.stats import pearsonr
import datetime
import PriceOrProduction as pp
import GetCPIWPI as cpi
import GetMonthData as md

TEXT_FILES = "output/sentiment"
INFLATION_OUT = "output/inflation"

import os
from sklearn import linear_model
from sklearn import feature_selection
import numpy as np
import scipy

def GetArticles(topic, month, year):
  return pickle.load(open("%s/%02d_%02d_%s_ratings.p" % (TEXT_FILES, month, year, topic), 'r'))

def Regression(X, y):
  lm = LinearRegression()
  lm.fit(X,y)
  params = np.append(lm.intercept_,lm.coef_)
  predictions = lm.predict(X)
  newX = pd.DataFrame({"Constant":np.ones(len(X))}).join(pd.DataFrame(X))
  MSE = (sum((y-predictions)**2))/(len(newX)-len(newX.columns))
  # Note if you don't want to use a DataFrame replace the two lines above with
  # newX = np.append(np.ones((len(X),1)), X, axis=1)
  # MSE = (sum((y-predictions)**2))/(len(newX)-len(newX[0]))
  var_b = MSE*(np.linalg.inv(np.dot(newX.T,newX)).diagonal())
  sd_b = np.sqrt(var_b)
  ts_b = params/ sd_b
  
  p_values =[2*(1-stats.t.cdf(np.abs(i),(len(newX)-1))) for i in ts_b]
  
  sd_b = np.round(sd_b,3)
  ts_b = np.round(ts_b,3)
  p_values = np.round(p_values,3)
  params = np.round(params,4)
  myDF3 = pd.DataFrame()
  myDF3["Coefficients"],myDF3["Standard Errors"],myDF3["t values"],myDF3["Probabilites"] = [params,sd_b,ts_b,p_values]
  #print(myDF3) 
  return myDF3, lm 

if __name__ == "__main__":
  month_ends, cpi, cpi_prev, inflation = cpi.GetAllInflation()
  month_ends_valid = md.GetAllMonthEnds()
  year_month = month_ends_valid[-1][0]*100 + month_ends_valid[-1][1]
  index = len(month_ends)
  print (year_month)
  for i, elem in enumerate(month_ends):
      if elem == year_month:
          index = i + 1
          break
  # print (len(month_ends), len(cpi), len(cpi_prev[0]), len(inflation[0]))
  print (index)
  print (month_ends)
  print (month_ends_valid)
  month_ends = month_ends[0:index]
  # raw_input()


  F = dict()
  p = dict()
  regression_data = dict()
  #topics = ["fuel", "food", "cloth", "house", "pan", "misc", "inflation"]
  topics = ["fuel", "food", "cloth", "misc", "inflation"]
  #topics = ["fuel", "food", "inflation"]
  count_defaulters = 0
  sum_sentiments_topics = dict()
  for topic in topics:
    sum_sentiments = list()
    for (i, month_end) in enumerate(month_ends):
      year = month_end // 100
      month = month_end % 100
      sum_sentiment = 0.0
      articles = GetArticles(topic, month, year)
      default_topic_score = 0.70
      for article, sentiment, _ in articles:
        sum_sentiment += sentiment * default_topic_score
	#sum_sentiment += sentiment
      sum_sentiments.append(sum_sentiment)
      print month_end, sum_sentiment, cpi[topic][i]
    sum_sentiments_topics[topic] = sum_sentiments
    print topic + " done"
    #print sum_sentiments_topics
    diff_cpi = [(100.0*(cpi[topic][i] - cpi_prev[topic][i])/cpi_prev[topic][i]) for i in xrange(1, len(month_ends))]
    diff_diff_cpi = [(0 if diff_cpi[i] - diff_cpi[i - 1] < 0 else 1) for i in xrange(1, len(diff_cpi))]
    diff_sum_sentiments = [(0 if sum_sentiments[i] - sum_sentiments[i-1] < 0 else 1) for i in xrange(2, len(sum_sentiments))]
    #X = np.asarray([[sum_sentiments[i], cpi[topic][i-1]] for i in xrange(1, len(cpi[topic]))])
    #X = np.asarray([[sum_sentiments[i], cpi_prev[topic][i]] for i in xrange(1, len(cpi[topic]))])
    X = np.asarray([[sum_sentiments[i]] for i in xrange(1, len(month_ends))])
    #Y = np.asarray(cpi[topic][1:])
    Y = np.asarray(diff_cpi)
    #print X, Y
    regression_data[topic], model = Regression(X,Y)
    #slope, intercept, r_value, p_value, std_err = stats.linregress(X, Y)
    plt.clf()
    #plt.xlabel("sentiment (rvalue = " + str(r_value) + ")")
    plt.xlabel("sentiment")
    plt.ylabel("delta cpi")
    plt.scatter(sum_sentiments[1:], diff_cpi[:])
    #print ("Slope %f, intercept %f, r_value %f, p_value %f, std_err %f" % (slope, intercept, r_value, p_value, std_err))
    #plt.scatter(sum_sentiments[1 : -1], diff_inflation[1:])
    plt.savefig("%s_prediction.png" % topic)
    plt.clf()
    plt.xlabel("index")
    plt.ylabel("sentiment_value")
    plt.plot([i for i in xrange(len(sum_sentiments))], sum_sentiments)
    plt.savefig("%s_sentiment.png" % topic)
    plt.clf()
    plt.xlabel("index")
    plt.ylabel("%s_inflation and sentiment value" % topic)
    line1 = plt.plot([i for i in xrange(len(sum_sentiments[1:]))], diff_cpi, 'g', label='inflation')
    line2 = plt.plot([i for i in xrange(len(sum_sentiments[1:]))], sum_sentiments[1:], 'r', label='sentiment')
    plt.legend()
    plt.title("%s" % topic, fontsize=26)
    plt.savefig("%s_inflation.png" % topic)
    plt.clf()
    plt.xlabel("index")
    plt.ylabel("%s_inflation_diff_value" % topic)
    plt.plot([i for i in xrange(len(sum_sentiments[2:]))], diff_diff_cpi, 'bo')
    plt.plot([i for i in xrange(len(sum_sentiments[2:]))], sum_sentiments[2:], 'ro')
    plt.savefig("%s_inflation_diff.png" % topic)
    plt.clf()
    plt.xlabel("index")
    plt.ylabel("%s_matchings" % topic)
    plt.plot([i for i in xrange(len(sum_sentiments[2:]))], [sum_sentiments[i + 2] - diff_diff_cpi[i] for i in xrange(len(diff_diff_cpi))], 'ro')
    plt.savefig("%s_matchings.png" % topic)
    plt.clf()
    plt.xlabel("sentiment_value")
    plt.ylabel("frequency")
    plt.hist(sum_sentiments)
    plt.savefig("%s_sentiment_histogram.png" % topic)
    F[topic], p[topic] = feature_selection.f_regression(X, Y)

  for topic in topics:
    #print topic, F[topic], p[topic]
    print topic
    print regression_data[topic]

  print "COULD NOT PARSE TOPIC SCORE: " + str(count_defaulters)
  diff_cpi = [(100.0*(cpi["inflation"][i] - cpi_prev["inflation"][i])/cpi_prev["inflation"][i]) for i in xrange(1, len(month_ends))]
  #diff_cpi = [np.log(cpi["inflation"][i]) - np.log(cpi_prev["inflation"][i]) for i in xrange(1, len(cpi["inflation"]))]
  diff_diff_cpi_raw = [diff_cpi[i] - diff_cpi[i-1] for i in xrange(1, len(diff_cpi))]
  diff_diff_cpi = [1 if elem >= 0.0 else 0 for elem in diff_diff_cpi_raw]
  X = np.asarray([[sum_sentiments_topics[topic][i] for topic in topics[:-1]] for i in xrange(1, len(month_ends))])
  month_ends = month_ends[1:]
  taper = 0
  X_ind = list()
  TAPER = 1.0 
  demo = False
  for month_end in month_ends:
      if month_end >= 201611:
          demo = True
      if month_end >= 201710 and TAPER >= 0.0:
          TAPER *= 0.5
      if not demo:
          X_ind.append(0)
      else:
          X_ind.append(TAPER)
  #X_ind = [0 if month_end < 201611 else 1 for month_end in month_ends]
  print X_ind
  #X_baseline = np.asarray([[inflation["inflation"][i - 1]] for i in xrange(1, len(cpi["inflation"]))])
  X_baseline = np.asarray([[inflation["inflation"][i - 1]] for i in xrange(1, len(month_ends))])
  #X = X_baseline
  #X = np.column_stack((X_baseline, X)) 
  Y = np.asarray(diff_cpi)
  print diff_diff_cpi_raw
  print diff_diff_cpi
  print sum_sentiments_topics["inflation"][2:]
  
  num_learn = 40 
  regress, model = Regression(np.column_stack((X, X_ind)), Y)
  print regress
  prediction = list()
  for i in xrange(num_learn, len(Y)):
    print month_ends[i]
    if month_ends[i] > 201611:
      X_pred = np.column_stack((X, X_ind))
    else:
      X_pred = X
    _, modell = Regression(X_pred[0:i], Y[0:i])
    prediction.extend(modell.predict(X_pred[i: i+1]))
  prediction = np.asarray(prediction)


  plt.clf()
  plt.xlabel("Prediction")
  plt.ylabel("True")
  plt.scatter(Y[num_learn:], prediction)
  plt.plot(xrange(2, 8), xrange(2, 8), 'g', label="Y=X")
  plt.title("Inflation Prediction", fontsize=26)
  plt.legend()
  plt.savefig("Overall Inflation Prediction.png")
  for i in xrange(len(prediction)):
    print "%f %f" %  (Y[num_learn + i], prediction[i])
    print month_ends[i + num_learn]
  print ""
  for i in xrange(len(prediction)):
    print "%f" %  (Y[num_learn + i])
  print ("\n\n\n\n\n")
  for i in xrange(len(prediction)):
    print "%f" %  (prediction[i])


 
  filename = "%s/%s.csv" % (INFLATION_OUT, datetime.date.today())
  outfile = open(filename, 'w')
  print ("Month True Predicted (Correl, p-value) upto this point")
  outfile.write("Month, True, Predicted, Correl, p-value\n")
  for i in xrange(1, len(prediction)):
      a = Y[num_learn: num_learn + i + 1]
      b = prediction[:i + 1]
      print ("%r %f %f %r" %  (month_ends[num_learn + i], Y[num_learn + i], prediction[i], pearsonr(a, b)))
      outfile.write("%r, %f, %f, %f, %f\n" %  (month_ends[num_learn + i], Y[num_learn + i], prediction[i], pearsonr(a, b)[0], pearsonr(a, b)[1]))
  outfile.close()
