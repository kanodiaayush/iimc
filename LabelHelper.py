import codecs
import re
import nltk
import sys

HEADLINE_SCORE = .9
FIRST_SENTENCE = .8
SENTENCE_TAPER = .04

WORDS_DIR = "data/words"
OUT_DIR = "output/topic_classified"

def WordInLine(line, words, stemmer):
  for token in nltk.word_tokenize(line):
    if stemmer.stem(token) in words:
      return True
  return False

def WordInLineBigramException(line, words, stemmer, bigram_exception):
  prev_stemmed = ""
  for token in nltk.word_tokenize(line):
    stemmed = stemmer.stem(token)
    for word in words:
      if stemmed == word:
        if word in bigram_exception.keys(): 
          if prev_stemmed not in bigram_exception[word]:
            return True
        else:
          return True
    prev_stemmed = stemmed
  return False

def RateArticle(headline, article, words, sno, bigram_exception):
  ans = 0.0
  debug = False
  if WordInLineBigramException(headline, words, sno, bigram_exception):
    ans += 0.9
  first_appearance = 1000
  count = 0
  for line in nltk.sent_tokenize(article):
    if WordInLineBigramException(line, words, sno, bigram_exception):
      first_appearance = min(first_appearance, count)
      break
    count += 1
    if count > 3:
      break
  ans += max(0, FIRST_SENTENCE - SENTENCE_TAPER * first_appearance)
  return min(ans, 1.0)

def ReadWords(filename, stemmer):
  f = open(filename, 'r')
  return [stemmer.stem(word.strip()) for word in f.readlines()]

def ReadWordsNoStemming(filename):
  f = open(filename, 'r')
  return [word.strip() for word in f.readlines()]

if __name__ == "__main__":
  sno = nltk.stem.SnowballStemmer('english')
  inputfile = sys.argv[1]
  f = open(inputfile, 'r')
  outfile = open(inputfile[0: inputfile.find('.')] + "_labelled.txt", 'a')

  words = dict()
  for topic in ["fuel", "food", "cloth", "house", "pan", "misc"]:
    words[topic] = ReadWords("%s/%s.txt" % (WORDS_DIR, topic), sno)
  count = 0
  for article in f.readlines():
    ratings = ""
    topics = ""
    for topic in ["fuel", "food", "cloth", "house", "pan", "misc"]:
      bigram_exception = dict()
      if topic == "fuel":
        exceptions = ReadWordsNoStemming("%s/%s_exception.txt" % (WORDS_DIR, topic))
        for word in exceptions:
          bigram_exception[sno.stem(word)] = ReadWords("%s/%s_%s.txt" % (WORDS_DIR, topic, word), sno)
      uarticle = article.decode('utf-8')
      pipe = uarticle.find("|||")
      headline = uarticle[0:pipe]
      body = uarticle[pipe + 4 : -1]
      rating = RateArticle(headline, body, words[topic], sno, bigram_exception)
      if rating > 0.6:
        topics += topic + " "
        ratings += str(rating) + " "
    #if topics == "":
      #topics += "none "
      #outfile.write(uarticle.encode('utf-8'))
      #outfile.write(topics)
      #outfile.write("\n")
      #continue
    if topics == "" or "fuel" in topics or "food" in topics:
      continue
    if topics == "misc ":
      print "\n\n\n\n\n\n\n\n\n\n\n"
      outfile.write(uarticle.encode('utf-8'))
      outfile.write(topics)
      outfile.write("\n")
      continue
    print "\n\n\n\n\n\n\n\n\n\n\n"
    print count
    count += 1
    print article
    print topics
    print ratings
    human_label = raw_input()
    if human_label == "exit" or human_label == "EXIT" or human_label == "Exit":
      broken = True
      break
    if human_label == "":
      human_label = topics.strip()
    topics = human_label
    outfile.write(uarticle.encode('utf-8'))
    outfile.write(topics)
    outfile.write("\n")

f.close()
outfile.close()

  
