import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB

import GetCPIWPI as infl
import GetMonthData as md

STEMFILE= 'data/all_word_stems.txt'

def HasPrefix(word, stems):
  return word.startswith(stems)

def LongestPrefix(word, stems):
  matches = [match for match in stems if word.startswith(match)]
  if matches:
    return max(matches, key=len)
  else:
    return ''

def Vectorize(articles, inflation):
  vectorizer = CountVectorizer(token_pattern = '[a-z][a-z]+', stop_words = 'english')
  X = vectorizer.fit_transform(articles).toarray()
  feature_names = vectorizer.get_feature_names()
  return X, inflation, feature_names

def ReadStems(stemfilename):
  stemfile = open(stemfilename, 'r')
  stems_list = list(str())
  for line in stemfile.read().splitlines():
    stems_list.append(line)
  stems = tuple(stems_list)
  return stems_list

def F_WordInStemsList(X, feature_names, stems):
  selected_columns = list()
  selected_features = list()
  for i in xrange(0, len(feature_names)):
    word = feature_names[i]
    if HasPrefix(word, stems):
      selected_columns.append(i)
      selected_features.append(word)
  X_new = np.zeros((X.shape[0], len(selected_columns)))
  #print X_new.shape
  #print X.shape
  j = 0
  for i in selected_columns:
    X_new[:, j] = X[:, i]
    j += 1
  print X_new.shape
  np.delete(X, selected_columns, 1)
  selected_features
  return X_new, selected_features

def F_StemsList(X, feature_names, stems):
  corresponding_stems = list()
  stem_indices = dict()
  start = 0
  for item in stems:
    stem_indices[item] = start
    start += 1
  for i in xrange(0, len(feature_names)):
    longest_prefix = LongestPrefix(feature_names[i], stems)
    index = -1
    if longest_prefix:
      index = stem_indices[longest_prefix]
    corresponding_stems.append(index)
  X_new = np.zeros((X.shape[0], len(stems)))
  for i in xrange(X.shape[0]):
    for j in xrange(X.shape[1]):
      if corresponding_stems[j] < 0:
        continue
      X_new[i][corresponding_stems[j]] += X[i][j]
  print X_new.shape
  return X_new, list(stems)

def Learn(X, y, learning_rows):
  clf = MultinomialNB()
  print y
  clf.fit(X[learning_rows], y[learning_rows])
  return clf

def Infer(X, y, inference_rows, clf):
  prediction = clf.predict(X[inference_rows]) 
  return prediction

def GetAccuracy(true_labels, prediction):
  print(prediction - true_labels)
  print(y[inference_rows])
  incorrect_labels = np.count_nonzero(prediction - true_labels)
  total_points = len(prediction)
  accuracy = float(total_points - incorrect_labels) / len(prediction)
  return accuracy

def GetConfusionMatrix(prediction, true_labels):
  confusion_matrix = np.zeros((2,2))
  for i in xrange(len(prediction)):
    x = 0 if true_labels[i] == 1 else 1
    y = 0 if prediction[i] == 1 else 1
    confusion_matrix[x][y] += 1
  return confusion_matrix

if __name__ == "__main__":
  articles, month_ends, inflation = md.GetAllData()
  print inflation
  X, y, feature_names = Vectorize(articles, inflation)
  stems = tuple(ReadStems(STEMFILE))
  #X, selected_features = F_WordInStemsList(X, feature_names, stems)
  X, selected_features = F_StemsList(X, feature_names, stems)
  tfidf = TfidfTransformer()
  tfidf.fit(X)
  
  
  #### Case 1 ##### 
  #print ("Training using the first 48 months, Inference on the rest")
  #num_train = 48
  #learning_rows = np.arange(0, num_train, 1)
  #inference_rows = np.arange(num_train, len(y), 1) 
  #clf = Learn(X, y, learning_rows)
  #print Infer(X, y, inference_rows, clf)
  
  ##### Case 2 #####
  #repeat = 10
  repeat = 200
  print ("Training using random 48 months, Inference on the rest, repeated %d times " % repeat)
  accuracies = list()
  confusion_matrix = np.zeros((2,2))
  for i in xrange(0, repeat):
    print "Iteration # %d" % i
    num_train = 46
    learning_rows = np.random.choice(len(y), num_train, replace=False)
    inference_rows = [i for i in xrange(len(y)) if i not in learning_rows]
    clf = Learn(X, y, learning_rows)
    true_labels = y[inference_rows]
    prediction = Infer(X, y, inference_rows, clf)
    accuracies.append(GetAccuracy(true_labels, prediction))
    confusion_matrix += GetConfusionMatrix(prediction, true_labels)
  
  confusion_matrix /= repeat
  print accuracies
  print (sum(accuracies)/ repeat)
  print (confusion_matrix)
  
  
