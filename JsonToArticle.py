import json
from collections import defaultdict
from ArticleSimilarity import GetUniqueArticles
import GetCPIWPI as infl
from GetConfig import UpdateMonths

BASE_DIR = "/storage/finlab_06"
OUT_DIR = "%s/data/month_20_10" % BASE_DIR
IN_DIR = "%s/data" % BASE_DIR
# yearmonths = {2018: range(1, 13, 1), 2017: range(
    # 1, 13, 1), 2016: range(1, 13, 1), 2015: range(1, 13, 1)}

def GetMonthArticles(month, year):
    articles = defaultdict(list)
    articlesjson = json.load(
        open('%s/EconomicTimes/output/json/local_%02d%d.json' % (IN_DIR, month, year)))
    for article in articlesjson:
        try:
            for elem in article['channel']:
                if "etmarkets" in elem.lower():
                    raise Exception('etmarkets')
            articleidlist = article['articleid']
            if len(articleidlist) > 0:
                articleid = articleidlist[0]
            else:
                articleid = str(99990000000000 + year *
                                1000000 + month * 10000 + idcount)
                idcount += 1
            thisarticle = ' '.join(article['headline']) + '|||' + \
                ' '.join(article['body']).lower()
            thisarticle = thisarticle.strip()
            thisarticle = ' '.join(thisarticle.split())
            day = int(article['datetime'][0][4:6])
        except Exception as e:
            # failed["%02d%d" % (month, year)] += 1
            continue
            #print (article)
            #print (e)
            # input()
        day = 0
        try:
            day = int(article['datetime'][0][4:6])
        except Exception as e:
            # no_dates_count += 1
            pass
        if thisarticle:
            articles[day].append(thisarticle)
    return articles


def articles_20_10(month, year):
    if month == 1:
        oldmonth = 12
        oldyear = year - 1
    else:
        oldmonth = month - 1
        oldyear = year
    # yearmonths = {i: range(1, 13, 1) for i in range(2012, 2015, 1)}
    ans = list()
    articles = GetMonthArticles(oldmonth, oldyear)
    for elem in range(20, 32, 1):
        daily_list = articles[elem]
        if not daily_list:
            continue
        daily_list, reduction = GetUniqueArticles(daily_list)
        ans.extend(daily_list)
    articles = GetMonthArticles(month, year)
    for elem in range(1, 11, 1):
        daily_list = articles[elem]
        if not daily_list:
            continue
        daily_list, reduction = GetUniqueArticles(daily_list)
        ans.extend(daily_list)
    outfile = open("%s/%d%02d_Economic_Times.txt" %
                   (OUT_DIR, year, month), 'w')
    outfile.write('\n'.join(ans).encode('utf-8'))
    outfile.close()

if __name__ == '__main__':
   months = UpdateMonths()
   for yearmonth in months:
       month = yearmonth % 100
       year = yearmonth / 100
       articles_20_10(month, year)
       print ("Done: %d %d" % (month, year))
