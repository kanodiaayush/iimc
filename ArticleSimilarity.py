# Get the unique articles out of a set of articles

import json
from sklearn.feature_extraction.text import CountVectorizer
from matplotlib import pyplot as plt
import numpy as np
import itertools as it
from collections import defaultdict

THRESHOLD = 38.0


def JaccardSimilarity(a, b):
    num = 0.0
    den = 0.0
    for x, y in zip(a, b):
        num += min(x, y)
        den += max(x, y)
    return (num/den)


def GetUniqueArticles(articles):
    vectorizer = CountVectorizer()
    cut_articles = list()
    for article in articles:
        splitarticle = article.split()
        cut_articles.append(
            ' '.join(splitarticle[0:min(len(splitarticle), 50)]))
    X = vectorizer.fit_transform(cut_articles).toarray()
    X = np.sqrt(X)
    features = vectorizer.get_feature_names()
    values = list()
    XT = X.T
    drop_indices = set()
    for i in range(X.shape[0]):
        if i in drop_indices:
            continue
        for j in range(i+1, X.shape[0], 1):
            if j in drop_indices:
                continue
            rowi = X[i]
            rowj = X[j]
            similarity = np.dot(rowi, rowj.T)
            if similarity >= THRESHOLD:
                drop_indices.add(j)
    for index in sorted(drop_indices, reverse=True):
        del articles[index]
    return articles, len(drop_indices)
    # print (len(values))
    # print (values[0:100])
    # print ("here")
    # values = [elem[0] for elem in values]
    # plt.hist(values, bins=10)
    # plt.show()


if __name__ == "__main__":
    articlesjson = json.load(open('EconomicTimes/output/json/012018.json'))
    articles = defaultdict(list)
    no_dates_count = 0
    for article in articlesjson:
        thisarticle = (
            ' '.join(article['headline']) + ' '.join(article['body'])).lower()
        # thisarticle = thisarticle.strip()
        splitarticle = thisarticle.split()
        thisarticle = ' '.join(splitarticle[0: min(len(splitarticle), 50)])
        # print (thisarticle)
        # input()
        day = 0
        try:
            day = int(article['datetime'][0][4:6])
        except Exception as e:
            no_dates_count += 1
        articles[day].append(thisarticle)
        # articles.append(' '.join(article['headline']))
    print("No dates for %d articles:" % no_dates_count)

    # vectorizer = CountVectorizer(token_pattern = '[a-z]+', binary = True)
