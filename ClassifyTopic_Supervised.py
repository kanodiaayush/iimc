import nltk
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB

import GetCPIWPI as infl
import GetMonthData as md

STEMFILE= 'data/all_word_stems.txt'
TRAININGDATA = 'data/labelled/topics/inputarticles_labelled.txt'
OUT_DIR = "output/topic_classified_supervised"

def HasPrefix(word, stems):
  return word.startswith(stems)

def LongestPrefix(word, stems):
  matches = [match for match in stems if word.startswith(match)]
  if matches:
    return max(matches, key=len)
  else:
    return ''

def VectorizeX(articles, vocab=None):
  vectorizer = CountVectorizer(token_pattern = '[a-z][a-z]+', stop_words = 'english', vocabulary=vocab, ngram_range=(1,2))
  print (len(articles))
  X = vectorizer.fit_transform(articles).toarray()
  feature_names = vectorizer.get_feature_names()
  return X, feature_names, vectorizer.vocabulary_

def ReadStems(stemfilename):
  stemfile = open(stemfilename, 'r')
  stems_list = list(str())
  for line in stemfile.read().splitlines():
    stems_list.append(line)
  stems = tuple(stems_list)
  return stems_list

def F_WordInStemsList(X, feature_names, stems):
  selected_columns = list()
  selected_features = list()
  for i in xrange(0, len(feature_names)):
    words = feature_names[i]
    for word in words.split(): 
      if HasPrefix(word, stems):
        selected_columns.append(i)
        selected_features.append(words)
        break
  X_new = np.zeros((X.shape[0], len(selected_columns)))
  #print X_new.shape
  #print X.shape
  j = 0
  for i in selected_columns:
    X_new[:, j] = X[:, i]
    j += 1
  print X_new.shape
  np.delete(X, selected_columns, 1)
  selected_features
  return X_new, selected_features

def F_StemsList(X, feature_names, stems):
  corresponding_stems = list()
  stem_indices = dict()
  start = 0
  for item in stems:
    stem_indices[item] = start
    start += 1
  for i in xrange(0, len(feature_names)):
    longest_prefix = LongestPrefix(feature_names[i], stems)
    index = -1
    if longest_prefix:
      index = stem_indices[longest_prefix]
    corresponding_stems.append(index)
  X_new = np.zeros((X.shape[0], len(stems)))
  for i in xrange(X.shape[0]):
    for j in xrange(X.shape[1]):
      if corresponding_stems[j] < 0:
        continue
      X_new[i][corresponding_stems[j]] += X[i][j]
  print X_new.shape
  return X_new, list(stems)

def Learn(X, y):
  clf = MultinomialNB()
  print y
  clf.fit(X, y)
  return clf

def Infer(X, clf):
  prediction = clf.predict(X) 
  return prediction

def GetAccuracy(true_labels, prediction):
  print(prediction - true_labels)
  print(y[inference_rows])
  incorrect_labels = np.count_nonzero(prediction - true_labels)
  total_points = len(prediction)
  accuracy = float(total_points - incorrect_labels) / len(prediction)
  return accuracy

def GetConfusionMatrix(prediction, true_labels):
  confusion_matrix = np.zeros((2,2))
  for i in xrange(len(prediction)):
    x = 0 if true_labels[i] == 1 else 1
    y = 0 if prediction[i] == 1 else 1
    confusion_matrix[x][y] += 1
  return confusion_matrix

TOPIC_LABELS = ["fuel", "food", "cloth", "house", "pan", "misc"]

def ArticleStart(text):
  text = text.decode('utf-8-sig')
  ans = ""
  pipe = text.find("|||")
  ans += text[0:pipe]
  ans += ans
  text = text[pipe+3:-1]
  count = 1
  num_lines = 1
  for line in nltk.sent_tokenize(text):
    ans += line
    count += 1
    if count > num_lines:
      break
  return ans

if __name__ == "__main__":
  train_articles = list()
  train_articles_body = list()
  train_labels = list()
  trainfile = open(TRAININGDATA, 'r')
  for i, line in enumerate(trainfile.readlines()):
    if i%2 == 0:
      train_articles.append(ArticleStart(line))
      #body = line[line.find("|||")+3: -1]
      
    else:
      for j, topic in enumerate(TOPIC_LABELS):
        if topic in line:
          train_labels.append(j)
          break
      else:
        train_labels.append(len(TOPIC_LABELS))

  #for i in xrange(len(train_labels)):
    #if train_labels[i] != 0:
      #train_labels[i] = 1
  #print train_articles, train_labels
  for i in xrange(len(TOPIC_LABELS) + 1):
    print len([j for j in train_labels if j == i])
  X_train, feature_names, vocab = VectorizeX(train_articles)
  y_train = np.asarray(train_labels)
  stems = tuple(ReadStems(STEMFILE))
  X_train, selected_features = F_StemsList(X_train, feature_names, stems)
  num_train = len(train_articles) 
  clf = Learn(X_train, y_train)
  print X_train.shape

  articles, month_ends, _= md.GetAllData()
  shards = 3
  for i, monthly_article in enumerate(articles):
    article_ratings = list()
    orig_article = [article for article in monthly_article.splitlines()]
    article_list = [ArticleStart(article) for article in monthly_article.splitlines()] 
    #print len(article_list)
    year, month = month_ends[i]
    X = np.zeros((0, len(stems)))
    lastindex = 0
    for shard in xrange(shards):
        nextindex = ((shard + 1) * len(article_list))//shards
        X_temp, feature_names, _ = VectorizeX(article_list[lastindex:nextindex], vocab)
        X_temp, _ = F_StemsList(X_temp, feature_names, stems)
        X = np.concatenate((X, X_temp), axis = 0)
        lastindex = nextindex
    #print "shapes"
    #print X.shape
    prediction = Infer(X, clf)
    f = dict()
    for topic in TOPIC_LABELS:
      f[topic] = open("%s/%02d_%02d_%s_ratings.txt" % (OUT_DIR, month, year, topic), 'w')
    for i, article in enumerate(orig_article):
      if prediction[i] < 6:
        f[TOPIC_LABELS[prediction[i]]].write(article + "\n")

    for topic in TOPIC_LABELS:
      f[topic].close()
    
    print month, year, "done"
