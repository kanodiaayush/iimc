
# coding: utf-8

# In[1]:

from selenium import webdriver
import time 
from selenium.webdriver.common.by import By
from bs4 import BeautifulSoup
import urllib as urllib2
import simplejson as json
import re
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
import os
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
import os


from GetConfig import UpdateMonths
chrome_options = Options()
chrome_options.add_argument("--start-maximized")
prefs = {"download.default_directory" : "/home/finlab/iimc/factiva/raw"}
chrome_options.add_experimental_option("prefs",prefs)
#driver = webdriver.Chrome(chromedriver)


# In[ ]:

def download_factiva_news(date, newspaper, foldername):
    prefs = {"download.default_directory" : foldername}
    chrome_options.add_experimental_option("prefs",prefs)
    day= str(date.day)
    month = str(date.month) 
    year= str(date.year)
    #print ("here")
    browser = webdriver.Chrome(chrome_options=chrome_options)
    #print ("here")
    browser.get('http://library.iimcal.ac.in/html/elecdball.php?lst=alp')
    actions = ActionChains(browser)
    browser.find_element_by_link_text('Factiva.com').click()
    time.sleep(5)
    browser.switch_to_window(browser.window_handles[1])
    time.sleep(25)
    element_to_hover_over= browser.find_element_by_link_text('Search')
    hover = ActionChains(browser).move_to_element(element_to_hover_over)
    hover.perform()
    browser.find_element_by_link_text('Search Builder').click()
    #print ("here")
    select = Select(browser.find_element_by_id('dr'))
    select.select_by_visible_text('Enter date range...')
    #print ("here 2")
    #raw_input()
    inputElement = browser.find_element_by_id('frd')
    inputElement.send_keys(day)
    inputElement = browser.find_element_by_id('frm')
    inputElement.send_keys(month)
    inputElement = browser.find_element_by_id('fry')
    inputElement.send_keys(year)
    inputElement = browser.find_element_by_id('tod')
    inputElement.send_keys(day)
    inputElement = browser.find_element_by_id('tom')
    inputElement.send_keys(month)
    inputElement = browser.find_element_by_id('toy')
    inputElement.send_keys(year)
    browser.find_element_by_id('scTab').click()
    inputElement = browser.find_element_by_id('scTxt')
    if newspaper == 'Economic Times':
        inputElement.send_keys(newspaper + ' India')
    else:
        inputElement.send_keys(newspaper)
    #raw_input()
    browser.implicitly_wait(3)
    browser.find_element_by_id('scLkp').click()
    browser.find_element_by_class_name('mnuItm').click()
    browser.find_element_by_id('reTab').click()
    inputElement= browser.find_element_by_link_text("Asia")
    inputElement.click()
    browser.implicitly_wait(5)
    time.sleep(5)
    browser.find_element_by_id('btnSBSearch').click()
    browser.implicitly_wait(5)
    browser.find_element_by_id('selectAll').click()
    time.sleep(5)
    browser.find_element_by_css_selector('.ppspdf').click()
    time.sleep(1)
    elem = WebDriverWait(browser, 5).until(EC.element_to_be_clickable((By.CSS_SELECTOR , '#listMenu-id-4 > li:nth-child(3) > a')))
    elem.click()
    time.sleep(35)
    browser.find_element_by_css_selector('#dj_header-wrap > ul.dj_header-nav.utility-nav.no-left-margin > li > a > span ').click()
    elem = WebDriverWait(browser, 5).until(EC.element_to_be_clickable((By.CSS_SELECTOR , '#dj_header-wrap > ul.dj_header-nav.utility-nav.no-left-margin > li > div > ul > li.logout.no-children > a')))
    #print (elem)
    elem.click()
    time.sleep(5)
    browser.quit()
    time.sleep(5)
    return



# In[ ]:

from datetime import datetime

months = UpdateMonths()
if len(months) != 1:
    raise Exception("Only one month can be crawled at a time")
yearmonth = months[0]
month = yearmonth % 100
year = yearmonth / 100
prev_month = month - 1
prev_year = year
if prev_month == 0:
    prev_month = 12
    prev_year -= 1
# The start date is crawled
start_date = "%d-%02d-10" % (prev_year, prev_month)
# The stop date is not crawled (1 - the stop date is crawled)
stop_date = "%d-%02d-11" % (year, month)
newspapers=['Economic Times', 'Business Line']
#newspapers=['Economic Times']
start = datetime.strptime(start_date, "%Y-%m-%d")
stop = datetime.strptime(stop_date, "%Y-%m-%d")
not_available=[]

for newspaper in newspapers:
    foldername = "/home/finlab/iimc/factiva/raw/%d/%s" % (yearmonth, newspaper)
    if not os.path.exists(foldername):
        os.makedirs(foldername)
    else:
        for filename in os.listdir(foldername):
            os.remove("%s/%s" % (foldername, filename))

from datetime import timedelta
while start < stop:
  for newspaper in newspapers:
    #download_factiva_news(start,newspaper)
    foldername = "/home/finlab/iimc/factiva/raw/%d/%s" % (yearmonth, newspaper)
    try:
        download_factiva_news(start,newspaper,foldername)
        print ("Done %r %s" % (start, newspaper))
    except Exception as e:
        not_available.append(start)
        print (" data Not available for today's date {} {} {} ".format(str(start.day),str(start.month),str(start.year)))
        print (e)
    #break
    time.sleep(10)
  start = start + timedelta(days=1) 
    


# In[ ]:

#listMenu-id-4 > li:nth-child(3) > a:nth-child(1)

