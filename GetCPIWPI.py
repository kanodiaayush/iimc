import csv
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from scipy import stats

def GetFractionalDelta(points):
  return [(points[i] - points[i - 1]) / points[i - 1] for i in xrange(1, len(points))]

def GetDeltaSign(points):
  return [(1 if (points[i] - points[i - 1] > 0) else -1) for i in xrange(1, len(points))]

def GetInflation():
  with open('data/CPI_WPI.csv') as csvfile:
    inforeader = csv.reader(csvfile, delimiter=',')
    df = pd.read_csv(csvfile)
  
    WPI = df.WPI
    CPI = df.CPI
    YoYInflation = df.Annual_inflation
    MONTH_END = df.MONTH_END
    WPI_Y = [WPI[i] for i in xrange(len(WPI))]
    CPI_Y = [CPI[i] for i in xrange(len(CPI))]
    YoYInflation_Y = [YoYInflation[i] for i in xrange(len(YoYInflation))]
    MONTH_END_Y = [MONTH_END[i] for i in xrange(len(MONTH_END))]
 
  Inflation_W_Delta = GetDeltaSign(GetFractionalDelta(WPI_Y))
  Inflation_C_Delta = GetDeltaSign(GetFractionalDelta(CPI_Y))
  YoYInflation_Delta = GetDeltaSign(YoYInflation_Y[1:])
  YoYInflation_Delta = GetDeltaSign(YoYInflation_Y[1:])
  return MONTH_END_Y[2:], Inflation_C_Delta, Inflation_W_Delta, YoYInflation_Delta

def GetAllInflation():
  with open('data/CPI_data.csv') as csvfile:
    inforeader = csv.reader(csvfile, delimiter=',')
    df = pd.read_csv(csvfile)
    MONTH_END = df.MONTH_END
    MONTH_END_Y = [MONTH_END[i] for i in xrange(len(MONTH_END))]
    CPI = dict()
    CPI_Y = dict()
    CPI_12_prev = dict()
    CPI_12_prev_Y = dict()
    Inflation = dict()
    Inflation_Y = dict()
    for topic in ["fuel", "food", "cloth", "house", "pan", "misc", "inflation"]:
      CPI[topic] = df["%s_CPI" % topic]
      CPI_Y[topic] = [CPI[topic][i] for i in xrange(len(CPI[topic]))]
      CPI_Y[topic] = CPI_Y[topic][2:]
      CPI_12_prev[topic] = df["%s_CPI_12_prev" % topic]
      CPI_12_prev_Y[topic] = [CPI_12_prev[topic][i] for i in xrange(len(CPI_12_prev[topic]))]
      CPI_12_prev_Y[topic] = CPI_12_prev_Y[topic][2:]
      Inflation[topic] = df["%s_Inflation" % topic]
      Inflation_Y[topic] = [Inflation[topic][i] for i in xrange(len(Inflation[topic]))]
      Inflation_Y[topic] = Inflation_Y[topic][2:]
  return MONTH_END_Y[2:], CPI_Y, CPI_12_prev_Y, Inflation_Y

if __name__ == "__main__":
  month_end, inflation, _, YoYInflation_crude = GetAllInflation()
  print month_end
  print len(month_end)
  print len(inflation["fuel"])
  #print len(inflation)
  #print len(YoYInflation_crude)
  #print YoYInflation_crude
  _, _, _, CPI_Y = GetAllInflation()
  #nums = [i for i in xrange(len(CPI_Y["all"]))]
  #plt.plot(nums, CPI_Y["all"])
  #plt.show()
  with open('data/CPI_data.csv') as csvfile:
    inforeader = csv.reader(csvfile, delimiter=',')
    df = pd.read_csv(csvfile)
    rbi = df['rbi_expectation']
    actual = df['inflation_Inflation']
    X = list()
    Y = list()
    for i in xrange(len(rbi)):
      if rbi[i] != 0.0:
        X.append(rbi[i])
        Y.append(actual[i])
    plt.scatter(X,Y)
    plt.xlabel("RBI expectation")
    plt.ylabel("Actual CPI")
    slope, intercept, r_value, p_value, std_err = stats.linregress(X,Y)
    print "Slope, intercept, rsquared, p, std_err"
    print slope, intercept, r_value * r_value, p_value, std_err
    plt.show()

