# Author : Sanjeev Kumar <sanjeevk2017@email.iimcal.ac.in>
import os, sys, re, csv
from pdfminer.pdfparser import PDFParser
from pdfminer.pdfdocument import PDFDocument, PDFNoOutlines
from pdfminer.pdfpage import PDFPage, PDFTextExtractionNotAllowed
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfdevice import PDFDevice
from pdfminer.converter import PDFPageAggregator
from pdfminer.layout import LAParams, LTTextBox, LTTextLine, LTFigure, LTImage
import binascii
from dateutil.parser import parse
from datetime import date

#TARGET_DIR = os.path.join(os.getcwd(),"DATA_NEWS")
TARGET_DIR = "/home/kanodiaayush/iimc/data/raw"
if not os.path.exists(TARGET_DIR):
    os.makedirs(TARGET_DIR)

def is_date(string):
	try:
		date = parse(string)
		return date
	except ValueError:
		return False

class ReportGenerator(object):
	def __init__(self,dest=TARGET_DIR):
		if not os.path.exists(dest):
			os.makedirs(dest)
		self.dest_dir = dest

	def generatefilepath(self,fname, extn=""):
		i = 1
		suffix = ""
		while(os.path.exists(os.path.join(self.dest_dir,fname+suffix+extn))):
			suffix = "_"+str(i)
			i +=1
		return (os.path.join(self.dest_dir,(fname+suffix+extn)))

	# data is expected as list of lists
	# header is expected as list of strings
	# filename is expected as string <text>
	def saveToCSV(self,filename,data,header=[]):
		
		# Remove if there is any extension
		filename = re.sub(r'\..*','',filename)

		# Generate the unique filename in the destination folder.
		fpath = self.generatefilepath(filename,".csv")

		with open(fpath,"wb") as f_ob:
			writer = csv.writer(f_ob)
			if header:
				writer.writerow(header)
			for row in data:
				writer.writerow(row)
		f_ob.close()
		print("File is saved as",fpath)

class ProcessFile:
	def __init__(self):
		# self.DATAFILE is list of filenames in self.DATADIR
		self.DATAFILE = ""
		self.DATADIR = ""
		'''try:
			if sys.platform.startswith('linux'):
				self.DATADIR = ""
			elif sys.platform.startswith('win'):
				self.DATADIR = ""
			else:
				print("\nPlatform Not supported\n")
		except ImportError:
			print("\nModule Import Error. Exiting\n")'''

	# Set the path of the driectory or file which is to be processed.
	# fpath is path of single file is type = "file", directory path is type = dir.
	def setPath(self,fpath):
		if fpath is not None:
			if os.path.isdir(fpath):
				self.DATAFILE = [os.path.join(fpath, fname) for fname in os.listdir(fpath)]
			else:
				print("\nRequested Path is not a Directory\n")
		else:
				print("\nInvalid File Path Parameter\n")

	# Return the file path set in private variables DATAFILE i.e list of filename
	def getPath(self):
		return self.DATAFILE

	# Read each file from the filepath set and return a key val pair i.e
	# key : FileName and Val : File content
	def readfile(self):
		pass

# Inherit ProcessFile
class ProcessPdf(ProcessFile):
	def __init__(self):
		try:
			ProcessFile.__init__(self)
			#ProcessFile.setPath(self,fpath)
		except ImportError,e:
			print("%s Module Import Error",str(e))
			sys.exit(1)

	def getToC(self,fpath):
		ProcessFile.setPath(self,fpath)
		for file in self.DATAFILE:
			with open(file,'rb') as fp:
				parser = PDFParser(fp)
				document = PDFDocument(parser, "")

				outlines = document.get_outlines()
				for (level,title,dest,a,se) in outlines:
					print(level,title, dest, a, se)
    		fp.close()

	# The functions returns the dictionary of dictionary
	# {"Filename":{"Page No":"content",...},...}
	def extractText(self,fpath):
		ProcessFile.setPath(self,fpath)
		TxtContent = {}
		try:
			#print(self.DATAFILE)
			for file in self.DATAFILE:
				# Get the filename from absolute path of the file
				fname = os.path.basename(file)
				print(file)
				with open(file,'rb') as fp:
					# Read the pdf object
					pdfObj = self.__fileReadPdf(fp,"")

					# read the pdf file content page by page in dictionar
					# Store the TxtContent Page no: Content in TxtContent File Name : <Page No : Content>
					TxtContent[fname] = self.__parsePages(pdfObj)	
				fp.close()

		except IOError,e:
				print("Unable to open File")
				sys.exit(1)
		return TxtContent

	# private module
	# Returns the pdf document object
	def __fileReadPdf(self,fp,file_pwd):
		document = None
		try:
			# create a pdf parser object associated with file object
			parser = PDFParser(fp)

			#create a pdf document object that stores the document structure
			document = PDFDocument(parser, file_pwd)

			# Check if document is extractable
			if not document.is_extractable:
				raise ValueError("PDF Text Extraction Not Allowed")
		except ValueError:
			document = None
			pass
		return document

	def __parsePages(self,docObj, imageDir = "Images"):
		rsrcmgr = PDFResourceManager()
		laparams = LAParams()
		device = PDFPageAggregator(rsrcmgr, laparams=laparams)
		interpreter = PDFPageInterpreter(rsrcmgr, device)

		text_content = {}
		for i, page in enumerate(PDFPage.create_pages(docObj)):
			interpreter.process_page(page)
			# receive the LTPage object for this page
			layout = device.get_result()
			# layout is an LTPage object which may contain child objects like LTTextBox, LTFigure, LTImage, etc.
			text_content[i+1] = self.__parse_lt_objs(layout, (i+1), imageDir)
		return text_content

	def __parse_lt_objs(self,lt_objs, page_number, imageDir):
		"""Iterate through the list of LT* objects and capture the text or image data contained in each"""
		text_content = []
		page_text = {} # k=(x0, x1) of the bbox, v=list of text strings within that bbox width (physical column)
		#print(page_number)
		for lt_obj in lt_objs:
			if isinstance(lt_obj, LTTextBox) or isinstance(lt_obj, LTTextLine):
				# text, so arrange is logically based on its column width
				#page_text = update_page_text_hash(page_text, lt_obj)
				#print(lt_obj.get_text())
				lt_obj_text = self.__to_bytestring(lt_obj.get_text())
				text_content.append(lt_obj_text)
				#print(lt_obj_text)

		text_content = filter(None,text_content)
		return ' '.join(text_content)

	def __to_bytestring(self,s, enc='utf-8'):
		if s:
			if not isinstance(s,str):
				s = s.encode(enc)
		return s


def ExtractNews(SRC_DIR):
	Topics = os.listdir(SRC_DIR)
	A = ProcessPdf()
	key = 1
	metaData = []
	for t in Topics:
                print t
		path1 = os.path.join(SRC_DIR,t)
		if os.path.isdir(path1):
			news_src = os.listdir(path1)
			for src in news_src:
				path2 = os.path.join(path1,src)
				if os.path.isdir(path2):
					news = os.listdir(path2)
					db = A.extractText(path2)
					for filename in news:
						path3 = os.path.join(path2, filename)
						print(path3)
						if os.path.exists("temp.txt"):
							os.remove("temp.txt")
						cmd = "cpdf -list-bookmarks "+path3+" >> temp.txt"
						os.system(cmd)
						pages = []
						title = []
						
						with open("temp.txt","r") as fb:
							content = fb.readlines()
							for line in content:
								headline = re.sub(r'(.*)\"(.+?)\"\s+(.*)',r'\2',line.strip())
								pageNo = int(re.sub(r'(.*)\"(.+?)\"\s+(.*)',r'\3',line.strip()))
								pages.append(pageNo)
								title.append(headline)
						fb.close()
						for i,p in enumerate(pages):
							try:
								print(p)
								content = ""
								if i < (len(pages) - 1):
									j = p
									while j < pages[i+1]:
										content += db[filename][j]
										j+=1
								else:
									content = db[filename][p]
								content = content.split("\n")
								date_str = ""
								d = False
								for k,line in enumerate(content):
									if re.match(r'copyright|\(c\)',line,flags=re.I) or k > 15:
										content[k] = ""
										break
									else:
										if len(line) >= 10 and line.strip() != title[i].strip():
											d = is_date(line)
											#print(line)
											if d != False:
												date_str = re.sub(r'-',"",date(d.year,d.month,d.day).isoformat())
										content[k] = ""
								content = ' '.join(content)
								content = re.sub(r'Document\s[a-z0-9]+\s*$','',content, flags= re.I)
								targetFname = t.upper()+"_"+date_str+"_"+src+"_"+str(key)+".txt"
								metaData.append([t.upper()+"_"+date_str+"_"+src+"_"+str(key),date_str,t.upper(),src,title[i]])
								fp = open(os.path.join(TARGET_DIR,targetFname),"w")
								fp.write(content)
								fp.close()
								key+=1
								#print(targetFname)
								print(title[i])
								#print(content)
							except KeyError:
								print("\nKey Error\n")
								print(i,p,path3)

	rep_ob = ReportGenerator()
	rep_ob.saveToCSV("metadata.csv",metaData,["KEY","DATE","TOPIC","SOURCE","HEADLINE"])


		
def main():
	print("Hello")
	A = ProcessPdf()
	#print(A.getPath())
	db = A.extractText(["NEWS_ANALYTICS","News","Inflation","ET","1.pdf"])
	
	#A.getToC(["NEWS_ANALYTICS","news.pdf"])
	pages = []
	title = []
	with open("bk1.txt","r") as fb:
		content = fb.readlines()
		for line in content:
			headline = re.sub(r'(.*)\"(.+?)\"\s+(.*)',r'\2',line.strip())
			pageNo = int(re.sub(r'(.*)\"(.+?)\"\s+(.*)',r'\3',line.strip()))
			pages.append(pageNo)
			title.append(headline)
			#print(line)
			#print(headline)
			#print(pageNo)
	fb.close()
	for i,p in enumerate(pages):
		print(p)
		content = ""
		if i < (len(pages) - 1):
			j = p
			while j < pages[i+1]:
				content += db["1.pdf"][j]
				j+=1
		else:
			content = db["1.pdf"][p]
		content = content.split("\n")
		for k,line in enumerate(content):
			if re.match(r'copyright|\(c\)',line,flags=re.I):
				content[k] = ""
				break
			else:
				date = is_date(line)
				if date != False:
					print(date.day, date.month, date.year)
				content[k] = ""
		content = ' '.join(content)
		print(title[i]+"\n")
		print(content)

if __name__ == '__main__':
	print("Hello")
	#main()
	#print(is_date("61 words"))
	ExtractNews("/home/kanodiaayush/iimc/data/raw")
