set nocompatible              " be iMproved, required
filetype off                  " required

filetype plugin indent on
syntax on

" Set the status bar to show the line number and column number
set number
set ruler

" If in insert mode, underline the current line
autocmd InsertEnter,InsertLeave * set cul!

" Navigation of split screens using alt and arrow keys
nmap <silent> <A-Up> :wincmd k<CR>
nmap <silent> <A-Down> :wincmd j<CR>
nmap <silent> <A-Left> :wincmd h<CR>
nmap <silent> <A-Right> :wincmd l<CR>
nmap <silent> <S-Up> :wincmd k<CR>
nmap <silent> <S-Down> :wincmd j<CR>
nmap <silent> <S-Left> :wincmd h<CR>
nmap <silent> <S-Right> :wincmd l<CR>

" Auto indent
set autoindent

" Setting mapleader
let mapleader="\\"

" Setting F3 to toggle comments using Nerd Commentor
map <F3> \c<space>

" Setting a default colour scheme; colorscheme default reverts to the default
" color scheme
syntax on

" To change the shape of the cursor in different modes
if has("autocmd")
  au InsertEnter * silent execute "!gconftool-2 --type string --set /apps/gnome-terminal/profiles/Default/cursor_shape ibeam"
  au InsertLeave * silent execute "!gconftool-2 --type string --set /apps/gnome-terminal/profiles/Default/cursor_shape block"
  au VimLeave * silent execute "!gconftool-2 --type string --set /apps/gnome-terminal/profiles/Default/cursor_shape ibeam"
endif

" Open split screens to the right and below
set splitright
set splitbelow

" Vertical line after 80 characters
set colorcolumn=+1

" Use space for tabs
set softtabstop=2 shiftwidth=2 tabstop=2 expandtab

" F3 to toggle comments
  map <F3> <plug>NERDCommenterToggle<CR>
  imap <F3> <Esc><plug>NERDCommenterToggle<CR>i 

" Ctrl + F to Format
map <C-F> :py3f /usr/share/vim/addons/syntax/clang-format-3.8.py<CR>
imap <C-F> <Esc>:py3f /usr/share/vim/addons/syntax/clang-format-3.8.py<CR>i
