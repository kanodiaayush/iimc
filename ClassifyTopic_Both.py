import re
import nltk

import GetMonthData as md

HEADLINE_SCORE = .9
FIRST_SENTENCE = .8
SENTENCE_TAPER = .04

WORDS_DIR = "data/words"
OUT_DIR = "output/topic_classified_both"
IN_DIR = "output/topic_classified_supervised"

def WordInLine(line, words, stemmer):
  for token in nltk.word_tokenize(line):
    if stemmer.stem(token) in words:
      return True
  return False

def WordInLineBigramException(line, words, stemmer, bigram_exception):
  prev_stemmed = ""
  for token in nltk.word_tokenize(line):
    stemmed = stemmer.stem(token)
    for word in words:
      if stemmed == word:
        if word in bigram_exception.keys(): 
          if prev_stemmed not in bigram_exception[word]:
            return True
        else:
          return True
    prev_stemmed = stemmed
  return False

def RateArticle(headline, article, words, sno, bigram_exception):
  ans = 0.0
  debug = False
  if WordInLineBigramException(headline, words, sno, bigram_exception):
    ans += 0.9
  first_appearance = 1000
  count = 0
  for line in nltk.sent_tokenize(article):
    if WordInLineBigramException(line, words, sno, bigram_exception):
      first_appearance = min(first_appearance, count)
      break
    count += 1
    if count > 3:
      break
  ans += max(0, FIRST_SENTENCE - SENTENCE_TAPER * first_appearance)
  return min(ans, 1.0)

def ReadWords(filename, stemmer):
  f = open(filename, 'r')
  return [stemmer.stem(word.strip()) for word in f.readlines()]

def ReadWordsNoStemming(filename):
  f = open(filename, 'r')
  return [word.strip() for word in f.readlines()]

if __name__ == "__main__":
  sno = nltk.stem.SnowballStemmer('english')
  articles, month_ends, inflation = md.GetAllData()
  words = dict()
  bigram_exception = dict()
  for topic in ["fuel", "food", "cloth", "house", "pan", "misc"]:
    words[topic] = ReadWords("%s/%s.txt" % (WORDS_DIR, topic), sno)
    if topic == "fuel":
      exceptions = ReadWordsNoStemming("%s/%s_exception.txt" % (WORDS_DIR, topic))
      for word in exceptions:
        bigram_exception[sno.stem(word)] = ReadWords("%s/%s_%s.txt" % (WORDS_DIR, topic, word), sno)
  
  for year, month in month_ends:
    for topic in ["fuel", "food", "cloth", "house", "pan", "misc"]:
      articlefile = open("%s/%02d_%04d_%s_ratings.txt" % (IN_DIR, month, year, topic), 'r')
      #print len(articlefile.readlines())
      article_ratings = list()
      article_list = list()
      for article in articlefile.readlines():
        article = article.decode('utf-8-sig')
        pipe = article.find("|||")
        headline = article[0:pipe]
        body = article[pipe + 4 : -1]
        rating = 0.0
        if topic == "fuel":
          rating = RateArticle(headline, body, words[topic], sno, bigram_exception)
        else:
          rating = RateArticle(headline, body, words[topic], sno, dict())
        article_list.append(article)
        article_ratings.append(rating)
          #break
      article_ratings, article_list = [list(t) for t in zip(*sorted(zip(article_ratings, article_list), reverse = True))]
      f = open("%s/%02d_%02d_%s_ratings.txt" % (OUT_DIR, month, year, topic), 'w')
      for i in xrange(0, len(article_list)):
        if article_ratings[i] < 0.75:
          break
        f.write("%d %f" % (i, article_ratings[i]))
        f.write(article_list[i].encode('utf8'))
        #f.write("%f\n" % article_ratings[i])
      f.close()
    print ("%02d %02d done" % (month, year))

  

  
