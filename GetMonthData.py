import numpy as np

import GetCPIWPI as infl
from GetConfig import UpdateMonths

BASE_DIR = "/storage/finlab_06/"
DATA_DIR = "%s/data/month_20_10" % BASE_DIR
# DATA_DIR = "data/month_20_10"

#def FilterInflationPrice(article):
  #if "inflation" not in article and "price" not in article:
    #return True
  #return False

#def FilterCrudeOil(article):
  #if "oil" not in article:
    #return True
  #return False

def GetMonthData(month, year, newspapers):
  ans = ""
  for newspaper in newspapers:
    f = open("%s/%04d%02d_%s.txt" % (DATA_DIR, year, month, newspaper), 'r')
    ans += f.read()
    ans += "\n"
  return ans
  #filtered_ans = ""
  #for article in ans.splitlines():
    #if not FilterCrudeOil(article):
    #filtered_ans += article + "\n"
  #return filtered_ans

def GetAllMonthEnds():
  MONTH_END, _ , _ , inflation = infl.GetAllInflation()
  month_ends = list()
  config_months = UpdateMonths()
  for month_end in MONTH_END:
    if month_end not in config_months:
      continue
    year = month_end // 100
    month = month_end % 100
    month_ends.append((year, month))
  return month_ends

def GetAllData(IDF=False):
  #Predicting YoY inflation delta
  MONTH_END, _ , _ , inflation = infl.GetAllInflation()
  #Predicting MoM inflation delta
  #MONTH_END, inflation, _ , _= infl.GetInflation()
  newspapers = ['Economic_Times']
  # newspapers = ['Business_Line', 'Economic_Times']
  #print MONTH_END
  articles = list(str())
  month_ends = list()
  config_months = UpdateMonths()
  if IDF:
      MONTH_END = MONTH_END[0:6]

  for month_end in MONTH_END:
  #for month_end in [201206, 201207]:
    if not IDF and month_end not in config_months:
      continue
    year = month_end // 100
    month = month_end % 100
    month_ends.append((year, month))
    articles.append(GetMonthData(month, year, newspapers))
  return articles, month_ends, np.asarray(inflation)

def GetMonthsAndInflation():
  MONTH_END, _ , _ , inflation = infl.GetAllInflation()
  month_ends = list()
  for month_end in MONTH_END:
    year = month_end // 100
    month = month_end % 100
    month_ends.append((year, month))
  return month_ends, np.asarray(inflation)


if __name__ == "__main__":
  data = GetMonthData(06,2014, ['Business_Line'])
  #print (data)
  print (len(data.splitlines()))
