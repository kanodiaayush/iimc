import nltk
import os
import pickle
import random as rand
import string

#TODO Pull functions used from here above
import ClassifyTopic as ct
import GetMonthData as md

WORDS_DIR = "data/words"
OUT_DIR = "output/price_or_production"
TEXT_FILES = "output/topic_classified_both"
TEXT_FILES_INFLATION = "output/topic_classified"

def WordInLineIndices(line, words, stemmer):
  ans = list()
  count = 0
  span_generator = nltk.WhitespaceTokenizer().span_tokenize(line)
  for a,b in span_generator:
    token = line[a:b]
    if stemmer.stem(token) in words:
      ans.append(count)
    count += 1
  return ans

def PrintLine(line, colour, f):
  text = '<span style="color:%s;font-size:15px" > %s </span>' % (colour, line)
  f.write(text.encode('utf8'))

def PrintArticle(article, f, sentence_sentiments=None):
  f.write("<p>")
  for i, (line, label, indices, price_indices, production_indices) in enumerate(article):
    line = string.replace(line, '<SCOPE>', '__S__')
    line = string.replace(line, '</SCOPE>', '__ES__')
    line = string.replace(line, '<NEG>', '__N__')
    line = string.replace(line, '</NEG>', '__EN__')
    if label == 0:
      PrintLine(line, 'black', f)
    elif label == 1:
      PrintLine(line, 'red', f)
    elif label == 2:
      PrintLine(line, 'blue', f)
    elif label == 3:
      PrintLine(line, 'green', f)
    if sentence_sentiments:
      if sentence_sentiments[i] != 0.0:
        f.write("%f" % sentence_sentiments[i])
    if price_indices:
      f.write("[")
      for price_index in price_indices:
        f.write("%d" % price_index)
      f.write("]")
    if production_indices:
      f.write("[")
      for production_index in production_indices:
        f.write("%d " % production_index)
      f.write("]")
    f.write("%s" % indices)
  f.write("</p>")

def PrintArticleNoIndices(article, f, sentence_sentiments=None):
  f.write("<p>")
  for i,  sent in enumerate(article):
    line, label, _, _ = sent
    if label == 0:
      PrintLine(line, 'black', f)
    elif label == 1:
      PrintLine(line, 'red', f)
    elif label == 2:
      PrintLine(line, 'blue', f)
    elif label == 3:
      PrintLine(line, 'green', f)
    if sentence_sentiments:
      if sentence_sentiments[i] != 0.0:
        f.write("%f" % sentence_sentiments[i])
  f.write("</p>")

def GetPriceOrProductionWords(topic):
  sno = nltk.stem.SnowballStemmer('english')
  print topic
  if topic == "inflation":
    price_file = "positives_inflation.txt"
    production_file = "negatives_inflation.txt"
  else:
    price_file = "price.txt"
    production_file = "production.txt"
  price = ct.ReadWords("%s/%s" % (WORDS_DIR, price_file), sno)
  production = ct.ReadWords("%s/%s" % (WORDS_DIR, production_file), sno)
  return price, production

def GetPriceOrProduction(line, price, production):
  sno = nltk.stem.SnowballStemmer('english')
  price_indices = WordInLineIndices(line, price, sno)
  price_topic = 1 if price_indices else 0
  production_indices = WordInLineIndices(line, production, sno)
  production_topic = 2 if production_indices else 0
  return (price_topic | production_topic), price_indices, production_indices
  #return rand.randint(0,3)

#TODO render html output in a separate file, only create pickle here
def RateFile(topic):
  month_ends = md.GetAllMonthEnds()
  price, production = GetPriceOrProductionWords(topic)
  print price
  print production
  for year, month in month_ends:
    if topic == "inflation":
      text_files = TEXT_FILES_INFLATION
    else:
      text_files = TEXT_FILES
    filebase = '%s/%02d_%02d_%s_ratings' % (text_files, month, year, topic)
    filename = filebase + '.txt'
    inputfile = open(filename, 'r')
    basename = os.path.basename(filebase)
    picklename = OUT_DIR + "/" + basename + ".p"
    f = open(OUT_DIR + "/" + basename + ".html", 'w')
    f.write("<head> %s <body>" % basename)
    articles_pickle = list(list())
    for article in inputfile.readlines():
      article = article.decode('utf-8-sig')
      article_pickle = list()
      pipe = article.find("|||") + 3
      first = article[0:pipe]
      label, price_indices, production_indices = GetPriceOrProduction(first, price, production)
      article_pickle.append((first + " .", label, price_indices, production_indices))
      for line in nltk.sent_tokenize(article[pipe : -1]):
        label, price_indices, production_indices = GetPriceOrProduction(line, price, production)
        sent = (line, label, price_indices, production_indices)
        article_pickle.append(sent)
      articles_pickle.append(article_pickle)
      PrintArticleNoIndices(article_pickle, f)
    f.write("</body> </head>")
    f.close()
    pickle.dump(articles_pickle, open(picklename, 'wb'))
    print ("%02d %02d %s done" % (month, year, topic))

if __name__ == "__main__":
  #for topic in ["house", "pan", "misc"]:
  for topic in ["fuel", "food", "cloth", "house", "pan", "misc","inflation"]:
  #for topic in ["fuel"]:
  #for topic in ["inflation"]:
    RateFile(topic)
